package com.employee.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.entity.EmployeeSwipe;

public interface EmployeeSwipeRepository extends JpaRepository<EmployeeSwipe, Long>{

	List<EmployeeSwipe> findBySwipeTimeBetween(LocalDateTime startOfDay, LocalDateTime endOfDay);

}
