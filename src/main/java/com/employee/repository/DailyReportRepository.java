package com.employee.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.entity.DailyReport;

public interface DailyReportRepository extends JpaRepository<DailyReport, Long>{

}
