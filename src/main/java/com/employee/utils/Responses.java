package com.employee.utils;

public interface Responses {

	/**
	 * Success Responses
	 */
	String DISPLAY_DATA_CODE = "SUCCESS001";
	String DISPLAY_DATA_MESSAGE = "Employee registered successfully";
	String SWIPE_TIME_CODE = "SUCCESS002";
	String SWIPE_TIME_MESSAGE = "Swipe details added successfully";
	String DETAILS_CODE = "SUCCESS003";
	String DETAILS_MESSAGE = "Employee details retrieved successfully";

	/**
	 * Exception Codes
	 */

	String EMPLOYEE_NOT_FOUND_CODE = "EX1002";
	String EMPLOYEE_NOT_FOUND_MESSAGE ="Employee Not Found";
	String EMPLOYEE_ALREADY_EXIST_CODE = "EX1001";
	String EMPLOYEE_ALREADY_EXIST_MESSAGE = "Employee already exists";
	String NO_SWIPES_FOUND_CODE = "EX1003";
	String NO_SWIPES_FOUND_MESSAGE = "No swipes found for this Id";
	String ADMIN_NOT_FOUND_CODE = "EX1004";
	String ADMIN_NOT_FOUND_MESSAGE = "Admin not found. Unauthorised Access";

}
