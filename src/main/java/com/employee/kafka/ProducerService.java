package com.employee.kafka;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;

import com.employee.dto.ReportDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ProducerService {
	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public void send(ReportDto reportDto) {

		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		Producer<String, ReportDto> kafkaProducer = new KafkaProducer<>(props, new StringSerializer(),
				new JsonSerializer());

		log.info("Message Sent: " + reportDto);
		ProducerRecord<String, ReportDto> reportRecord = new ProducerRecord<>("endOfDayReportTopic", reportDto);
		
	}
}
