package com.employee;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.employee.entity.Employee;
import com.employee.entity.EmployeeSwipe;
import com.employee.repository.EmployeeRepository;
import com.employee.repository.EmployeeSwipeRepository;

@SpringBootApplication
@EnableScheduling
public class EmployeeServiceApplication implements CommandLineRunner {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	EmployeeSwipeRepository employeeSwipeRepository;

	public static void main(String[] args) {
		SpringApplication.run(EmployeeServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		addEmployee();
		addAdmin();
		addEmployeeSwipe();
	}

	private void addEmployeeSwipe() {
		List<EmployeeSwipe> employeeSwipes = new ArrayList<>();
		employeeSwipes.add(new EmployeeSwipe(1l, 1l, new Employee(1l, "keerthana", "Kee@gmail"),
				LocalDateTime.now().minusHours(7)));
		employeeSwipes.add(new EmployeeSwipe(2l, 1l, new Employee(1l, "keerthana", "Kee@gmail"),
				LocalDateTime.now().minusHours(5)));
		employeeSwipes.add(new EmployeeSwipe(3l, 1l, new Employee(1l, "keerthana", "Kee@gmail"),
				LocalDateTime.now().minusHours(1)));
		employeeSwipes.add(new EmployeeSwipe(4l, 1l, new Employee(1l, "keerthana", "Kee@gmail"), LocalDateTime.now()));
		employeeSwipes.add(new EmployeeSwipe(5l, 2l, new Employee(2l, "cynthia", "cyn@gmail"), LocalDateTime.now()));
		employeeSwipes.add(
				new EmployeeSwipe(6l, 2l, new Employee(2l, "cynthia", "cyn@gmail"), LocalDateTime.now().minusHours(2)));
		employeeSwipes.add(
				new EmployeeSwipe(7l, 2l, new Employee(2l, "cynthia", "cyn@gmail"), LocalDateTime.now().minusHours(8)));

		employeeSwipeRepository.saveAll(employeeSwipes);

	}

	private void addAdmin() {
		// TODO Auto-generated method stub

	}

	private void addEmployee() {
		List<Employee> employees = new ArrayList<>();
		employees.add(new Employee(1l, "keerthana", "Kee@gmail"));
		employees.add(new Employee(2l, "cynthia", "cyn@gmail"));
		employees.add(new Employee(3l, "Vijay", "vija@gmail"));
		employeeRepository.saveAll(employees);

	}

}
