package com.employee.service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.employee.dto.ReportDto;
import com.employee.entity.EmployeeSwipe;
import com.employee.kafka.ProducerService;
import com.employee.repository.EmployeeSwipeRepository;

@Service

public class EmployeeSwipeService  {

	private final EmployeeSwipeRepository swipeRepository;
	private final ProducerService producerService;

	public EmployeeSwipeService(EmployeeSwipeRepository swipeRepository,
			ProducerService producerService) {
		super();
		this.swipeRepository = swipeRepository;
		this.producerService = producerService;
	}

	@Scheduled(fixedRate = 15000)
	public void calculateAndSaveWorkingHours() {
		LocalDateTime startOfDay = LocalDate.now().atStartOfDay();
		LocalDateTime endOfDay = LocalDate.now().atTime(LocalTime.MAX);
		List<EmployeeSwipe> swipes = swipeRepository.findBySwipeTimeBetween(startOfDay, endOfDay);
		Map<Long, List<EmployeeSwipe>> swipesByEmployee = swipes.stream()
				.collect(Collectors.groupingBy(EmployeeSwipe::getEmployeeId));
		for (Map.Entry<Long, List<EmployeeSwipe>> entry : swipesByEmployee.entrySet()) {
			Long employeeId = entry.getKey();
			List<EmployeeSwipe> employeeSwipes = entry.getValue();
			LocalDateTime firstSwipe = employeeSwipes.stream().map(EmployeeSwipe::getSwipeTime)
					.min(LocalDateTime::compareTo).orElse(null);
			LocalDateTime lastSwipe = employeeSwipes.stream().map(EmployeeSwipe::getSwipeTime)
					.max(LocalDateTime::compareTo).orElse(null);
			if (firstSwipe != null && lastSwipe != null) {
				Duration duration = Duration.between(firstSwipe, lastSwipe);
				long totalHours = duration.toHours();
				ReportDto reportDto = new ReportDto();
				reportDto.setEmployeeId(employeeId);
				reportDto.setDate(startOfDay.toString());
				reportDto.setTotalHours(totalHours);
				producerService.send(reportDto);
			}

		}

	}

}
