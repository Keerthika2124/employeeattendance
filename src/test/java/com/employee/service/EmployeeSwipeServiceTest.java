package com.employee.service;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.employee.dto.ReportDto;
import com.employee.entity.Employee;
import com.employee.entity.EmployeeSwipe;
import com.employee.kafka.ProducerService;
import com.employee.repository.EmployeeSwipeRepository;

@SpringBootTest
public class EmployeeSwipeServiceTest {

	@Mock
	EmployeeSwipeRepository swipeRepository;

	@Mock
	ProducerService producerService;

	@InjectMocks
	EmployeeSwipeService employeeSwipeService;

	@Test
	void EmployeeSwipe() {
		LocalDateTime startOfDay = LocalDate.now().atStartOfDay();
		LocalDateTime endOfDay = LocalDate.now().atTime(LocalTime.MAX);
		Employee employee = new Employee(1l, "karthika", "kar@hcl");
		List<EmployeeSwipe> employeeSwipes = new ArrayList<>();
		ReportDto reportDto = new ReportDto(1l, startOfDay.toString(), 11l);
		employeeSwipes.add(new EmployeeSwipe(1l, 1l, employee, startOfDay.plusHours(8)));
		employeeSwipes.add(new EmployeeSwipe(1l, 1l, employee, startOfDay.plusHours(19)));
		when(swipeRepository.findBySwipeTimeBetween(startOfDay, endOfDay)).thenReturn(employeeSwipes);
		employeeSwipeService.calculateAndSaveWorkingHours();
		doNothing().when(producerService).send(reportDto);
		verify(producerService, times(1)).send(reportDto);
	}

	@Test
	void EmployeeSwipe_NoData() {
		LocalDateTime startOfDay = LocalDate.now().atStartOfDay();
		LocalDateTime endOfDay = LocalDate.now().atTime(LocalTime.MAX);
		ReportDto reportDto = new ReportDto();
		when(swipeRepository.findBySwipeTimeBetween(startOfDay, endOfDay)).thenReturn(Collections.emptyList());
		employeeSwipeService.calculateAndSaveWorkingHours();
		verify(producerService, never()).send(reportDto);
	}
	
	@Test
	void EmployeeSwipeNoSwipeOut() {
		LocalDateTime startOfDay = LocalDate.now().atStartOfDay();
		LocalDateTime endOfDay = LocalDate.now().atTime(LocalTime.MAX);
		Employee employee = new Employee(1l, "karthika", "kar@hcl");
		List<EmployeeSwipe> employeeSwipes = new ArrayList<>();
		ReportDto reportDto = new ReportDto(1l, startOfDay.toString(), 11l);
		employeeSwipes.add(new EmployeeSwipe(1l, 1l, employee, startOfDay.plusHours(8)));
		when(swipeRepository.findBySwipeTimeBetween(startOfDay, endOfDay)).thenReturn(employeeSwipes);
		employeeSwipeService.calculateAndSaveWorkingHours();
		verify(producerService, never()).send(reportDto);
	}
	
	
	
}
